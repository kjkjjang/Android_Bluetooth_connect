package com.example.jk.sampleapplication;

import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class BluetoothConnectThread extends Thread {
    final int RECIEVE_MESSAGE = 1;        // Status  for Handler

    private final InputStream mmInStream;
    private final OutputStream mmOutStream;

    // up , left, right, down, left spin, right spin, stop
    private boolean[] signalArray;
    private String writeMessage;

    private Handler h;

    private int carSpeed;
    private StreeingWheelRotationActivity.SpeedMotion motionType;
    private double carAngle;
    private boolean isConnected;

    public BluetoothConnectThread(BluetoothSocket socket) {
        InputStream tmpIn = null;
        OutputStream tmpOut = null;

        // Get the input and output streams, using temp objects because
        // member streams are final
        try {
            tmpIn = socket.getInputStream();
            tmpOut = socket.getOutputStream();
            this.h = h;
        } catch (IOException e) { }

        mmInStream = tmpIn;
        mmOutStream = tmpOut;
        signalArray = new boolean[7];

        motionType = StreeingWheelRotationActivity.SpeedMotion.NOTHING;
        carAngle = 0;
        isConnected = true;
    }

    @Override
    public void run() {
        while (isConnected) {
            try {
                if (carSpeed == 0 && motionType == StreeingWheelRotationActivity.SpeedMotion.NOTHING) {
                    Thread.sleep(100);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (motionType == StreeingWheelRotationActivity.SpeedMotion.ACCELERATE) {
                this.carSpeed += 5;
            } else if (motionType == StreeingWheelRotationActivity.SpeedMotion.STOP) {
                this.carSpeed -= 10;
            } else {
                this.carSpeed -= 3;
            }
            if (this.carSpeed > 255) {
                this.carSpeed = 255;
            } else if (this.carSpeed < 0) {
                this.carSpeed = 0;
            }

            StringBuffer sb = new StringBuffer();

            long leftSpeed, rightSpeed;
            double changeSpeed;
            changeSpeed = (carSpeed / 255.0) * 255.0;
            if (carAngle == 0) {
                // 자동차 왼쪽 바퀴 1 * 스피드
                // 자동차 오른쪽 바퀴 1 * 스피드
                leftSpeed = Math.round(changeSpeed);
                rightSpeed = Math.round(changeSpeed);

            } else if (carAngle > 0) { // 오른쪽 회전
                // 자동차 왼쪽 바퀴 1 * 스피드
                // 자동차 오른쪽 바퀴 ((90-각도)/90) * 스피드
                leftSpeed = Math.round(changeSpeed);
                rightSpeed = (int) (( (90.0 - carAngle) / 90.0) * Math.round(changeSpeed) );
            } else { // 왼쪽 회전
                // 자동차 오른쪽 바퀴 1 * 스피드
                // 자동차 왼쪽 바퀴 ((90+각도)/90) * 스피드
                rightSpeed = Math.round(changeSpeed);
                leftSpeed = Math.round ( ( (90.0 + carAngle) / 90.0) * Math.round(changeSpeed));
            }
            sb.append(String.format("L%03dR%03d", leftSpeed, rightSpeed));

            sb.append('\n');
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


//            Log.d("Car Speed", "[" + this.carSpeed + "]change[" + changeSpeed + "][" + sb.toString() + "]");
            write(sb.toString());
        }
    }

    public void setMotion(StreeingWheelRotationActivity.SpeedMotion motionType) {
        this.motionType = motionType;
    }

    public void setAngle(double carAngle) {
        this.carAngle = carAngle;
    }


    /* Call this to send data to the remote device */
    public void write(String message) {
        Log.d("write", "...Data to send: " + message + "...");
        byte[] msgBuffer = message.getBytes();
        try {
            mmOutStream.write(msgBuffer);
        } catch (IOException e) {
            Log.d("write error", "...Error data send: " + e.getMessage() + "...");
            isConnected = false;
        }
    }

}
