package com.example.jk.sampleapplication;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private Button leftSpinButton;
    private Button rightSpinButton;

    private Button stopButton;

    private Button leftButton;
    private Button rightButton;
    private Button upButton;
    private Button downButton;


    Handler h;


    final int RECIEVE_MESSAGE = 1;        // Status  for Handler
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private StringBuilder sb = new StringBuilder();
    private static int flag = 0;

    private ConnectedThread mConnectedThread;

    // SPP UUID service
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    // MAC-address of Bluetooth module (you must edit this line)
    //private static String address = "20:16:03:08:64:39";
    private static String address = "AB:AE:92:56:34:02";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        h = new Handler() {
            public void handleMessage(android.os.Message msg) {
                switch (msg.what) {
                    case RECIEVE_MESSAGE:
                        byte[] readBuf = (byte[]) msg.obj;
                        String strIncom = new String(readBuf, 0, msg.arg1);
                        sb.append(strIncom);
                        int endOfLineIndex = sb.indexOf("\r\n");
                        if (endOfLineIndex > 0) {
                            String sbprint = sb.substring(0, endOfLineIndex);
                            sb.delete(0, sb.length());
                            Log.d("Data from Arduino[" + sbprint + "]" , "flag[" + flag + "][" + (flag%4) + "]");
                            flag++;
                        }
                        break;
                }
            };
        };

        leftButton = (Button) findViewById(R.id.left_button);
        rightButton = (Button) findViewById(R.id.right_button);
        upButton = (Button) findViewById(R.id.up_button);
        downButton = (Button) findViewById(R.id.down_button);
        leftSpinButton = (Button) findViewById(R.id.left_spin_button);
        rightSpinButton = (Button) findViewById(R.id.right_spin_button);
        stopButton = (Button) findViewById(R.id.stop_button);


        leftSpinButton.setOnTouchListener((View v, MotionEvent event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
//                mConnectedThread.write("L");
                mConnectedThread.addState(MOTOR_SIGNAL.LEFT_SPIN);
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                mConnectedThread.removeState(MOTOR_SIGNAL.LEFT_SPIN);
            }
            return true;
        });

        rightSpinButton.setOnTouchListener((View v, MotionEvent event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
//                mConnectedThread.write("L");
                mConnectedThread.addState(MOTOR_SIGNAL.RIGHT_SPIN);
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                mConnectedThread.removeState(MOTOR_SIGNAL.RIGHT_SPIN);
            }
            return true;
        });

        // send F - front, B - back, L - left, R - right
        leftButton.setOnTouchListener((View v, MotionEvent event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
//                mConnectedThread.write("L");
                mConnectedThread.addState(MOTOR_SIGNAL.LEFT);
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                mConnectedThread.removeState(MOTOR_SIGNAL.LEFT);
            }
            return true;
        });
        rightButton.setOnTouchListener((View v, MotionEvent event) -> {
//            mConnectedThread.write("R");
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                mConnectedThread.addState(MOTOR_SIGNAL.RIGHT);
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                mConnectedThread.removeState(MOTOR_SIGNAL.RIGHT);
            }

            return true;
        });
        upButton.setOnTouchListener((View v, MotionEvent event) -> {
//            mConnectedThread.write("F");
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                mConnectedThread.addState(MOTOR_SIGNAL.UP);
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                mConnectedThread.removeState(MOTOR_SIGNAL.UP);
            }

            return true;
        });
        downButton.setOnTouchListener((View v, MotionEvent event) -> {
//            mConnectedThread.write("B");
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                mConnectedThread.addState(MOTOR_SIGNAL.DOWN);
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                mConnectedThread.removeState(MOTOR_SIGNAL.DOWN);
            }

            return true;
        });


        btAdapter = BluetoothAdapter.getDefaultAdapter();       // get Bluetooth adapter
        checkBTState();

    }



    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
        if(Build.VERSION.SDK_INT >= 10){
            try {
                final Method m = device.getClass().getMethod("createInsecureRfcommSocketToServiceRecord", new Class[] { UUID.class });
                return (BluetoothSocket) m.invoke(device, MY_UUID);
            } catch (Exception e) {
                Log.e("bluetooth socket err", "Could not create Insecure RFComm Connection",e);
            }
        }
        return  device.createRfcommSocketToServiceRecord(MY_UUID);
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.d("onResume", "...onResume - try connect...");

        // Set up a pointer to the remote node using it's address.
        BluetoothDevice device = btAdapter.getRemoteDevice(address);

        // Two things are needed to make a connection:
        //   A MAC address, which we got above.
        //   A Service ID or UUID.  In this case we are using the
        //     UUID for SPP.

        try {
            btSocket = createBluetoothSocket(device);
        } catch (IOException e) {
            errorExit("Fatal Error", "In onResume() and socket create failed: " + e.getMessage() + ".");
        }

        // Discovery is resource intensive.  Make sure it isn't going on
        // when you attempt to connect and pass your message.
        btAdapter.cancelDiscovery();

        // Establish the connection.  This will block until it connects.
        Log.d("on resume bluetooth", "...Connecting...");
        try {
            btSocket.connect();
            Log.d("on resume bluetooth", "Connect ok");
        } catch (IOException e) {
            try {
                btSocket.close();
            } catch (IOException e2) {
                errorExit("Fatal Error", "In onResume() and unable to close socket during connection failure" + e2.getMessage() + ".");
            }
        }

        // Create a data stream so we can talk to server.
        Log.d("bluetooth connected", "...Create Socket...");

        mConnectedThread = new ConnectedThread(btSocket);
        mConnectedThread.start();
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.d("on pause", "...In onPause()...");

        try     {
            btSocket.close();
        } catch (IOException e2) {
            errorExit("Fatal Error", "In onPause() and failed to close socket." + e2.getMessage() + ".");
        }
    }

    private void checkBTState() {
        // Check for Bluetooth support and then check to make sure it is turned on
        // Emulator doesn't support Bluetooth and will return null
        if(btAdapter==null) {
            errorExit("Fatal Error", "Bluetooth not support");
        } else {
            if (btAdapter.isEnabled()) {
                Log.d("bluetooth enabled", "...Bluetooth ON...");
            } else {
                //Prompt user to turn on Bluetooth
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }
    }

    private void errorExit(String title, String message){
        Toast.makeText(getBaseContext(), title + " - " + message, Toast.LENGTH_LONG).show();
        finish();
    }


    public enum MOTOR_SIGNAL {
        UP(0), DOWN(1), LEFT(2), RIGHT(3), LEFT_SPIN(4), RIGHT_SPIN(5), STOP(6);

        final int value;

        private MOTOR_SIGNAL(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        // up , left, right, down, left spin, right spin, stop
        private boolean[] signalArray;
        private String writeMessage;

        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
            signalArray = new boolean[7];
        }

        public void run() {
            byte[] buffer = new byte[256];  // buffer store for the stream
            int bytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                    // Read from the InputStream
                    bytes = mmInStream.read(buffer);        // Get number of bytes and message in "buffer"
                    h.obtainMessage(RECIEVE_MESSAGE, bytes, -1, buffer).sendToTarget();     // Send to message queue Handler
                } catch (IOException e) {
                    break;
                }
            }
        }

        public void addState(MOTOR_SIGNAL signal) {
            signalArray[signal.getValue()] = true;
            sendMessage();
        }

        public void removeState(MOTOR_SIGNAL signal) {
            signalArray[signal.getValue()] = false;
            sendMessage();
        }

        private void sendMessage() {

            // send F - front, B - back, L - left, R - right
            if (signalArray[MOTOR_SIGNAL.STOP.getValue()]) {
                this.write("S");
            } else if (signalArray[MOTOR_SIGNAL.LEFT_SPIN.getValue()]) {
                this.write("L");
            } else if (signalArray[MOTOR_SIGNAL.RIGHT_SPIN.getValue()]) {
                this.write("R");
            } else {
                // left, right, up, down 조합
                // up + left
                if (signalArray[MOTOR_SIGNAL.LEFT.getValue()] && signalArray[MOTOR_SIGNAL.UP.getValue()]) {
                    this.write("Q");
                }
                // up + right
                else  if (signalArray[MOTOR_SIGNAL.RIGHT.getValue()] && signalArray[MOTOR_SIGNAL.UP.getValue()]) {
                    this.write("E");
                }
                // down + left
                else if (signalArray[MOTOR_SIGNAL.LEFT.getValue()] && signalArray[MOTOR_SIGNAL.DOWN.getValue()]) {
                    this.write("Z");
                }
                // down + right
                else if (signalArray[MOTOR_SIGNAL.RIGHT.getValue()] && signalArray[MOTOR_SIGNAL.DOWN.getValue()]) {
                    this.write("C");
                }
                // up
                else if (signalArray[MOTOR_SIGNAL.UP.getValue()]) {
                    this.write("F");
                }
                // down
                else if (signalArray[MOTOR_SIGNAL.DOWN.getValue()]) {
                    this.write("B");
                }
                // stop
                else {
                    this.write("S");
                }
            }
        }

        /* Call this from the main activity to send data to the remote device */
        private void write(String message) {
            Log.d("write", "...Data to send: " + message + "...");
            byte[] msgBuffer = message.getBytes();
            try {
                mmOutStream.write(msgBuffer);
            } catch (IOException e) {
                Log.d("write error", "...Error data send: " + e.getMessage() + "...");
            }
        }

    }


}
