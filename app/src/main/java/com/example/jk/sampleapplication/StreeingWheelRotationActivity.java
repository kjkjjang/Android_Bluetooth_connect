package com.example.jk.sampleapplication;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.UUID;

/**
 *  스피드 조절 공식
 *  - 각도는 -90 ~ 90 (y 축 즉 정면이 0도가 기준이 된다)
 *  - 스피드는 carSpeedThread 중 carSpeed 변수로 활용
 *  - 각도가 양수 (x > 0)일 때 (오른쪽 방향 커브)
 *    - 좌: 1.0 * 스피드
 *    - 우: ((90 - 각도)/90) * 스피드
 *  - 각도가 음수 (x < 0)일 때 (왼쪽 방향 커브)
 *    - 우: 1.0 * 스피드
 *    - 좌: ((90 + 각도)/90) * 스피드
 */
public class StreeingWheelRotationActivity extends AppCompatActivity implements View.OnTouchListener {

    public enum SpeedMotion {
        ACCELERATE, STOP, NOTHING;
    }

    private ImageView wheel;
    // 좌표 상 실제로 가리키는 앵글
    private double mCurrAngle = 0;
    // 이전 앵글
    private double mPrevAngle = 0;
    // 화면에 뿌려줄 앵글
    private double nowAngle = 0;
    // 이동한 앵글 (mCurrAngle - mPrevAngle)
    private double moveAngle = 0;

//    ImageView bask;

    private ImageButton acceleratorPedal;
    private ImageButton stopPedal;

    private BluetoothConnectThread bluetoothConnectThread;


    final int RECIEVE_MESSAGE = 1;        // Status  for Handler
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private StringBuilder sb = new StringBuilder();
    private static int flag = 0;

    // SPP UUID service
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    // MAC-address of Bluetooth module (you must edit this line)
    //private static String address = "20:16:03:08:64:39";
    private static String address = "AB:AE:92:56:34:02";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.streeing_wheel);

        wheel = (ImageView)findViewById(R.id.streeing_wheel_image);
        wheel.setOnTouchListener(this);

        btAdapter = BluetoothAdapter.getDefaultAdapter();       // get Bluetooth adapter
        checkBTState();

        acceleratorPedal = (ImageButton) findViewById(R.id.accelerate);
        stopPedal = (ImageButton) findViewById(R.id.stop);

        acceleratorPedal.setBackgroundResource(R.drawable.pedal_acc01);
        stopPedal.setBackgroundResource(R.drawable.pedal_brea01);

        acceleratorPedal.setOnTouchListener((View v, MotionEvent event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
//                Log.d("accelerator button", "down");
                acceleratorPedal.setBackgroundResource(R.drawable.pedal_acc02);
                bluetoothConnectThread.setMotion(SpeedMotion.ACCELERATE);
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
//                Log.d("accelerator button", "up");
                acceleratorPedal.setBackgroundResource(R.drawable.pedal_acc01);
                bluetoothConnectThread.setMotion(SpeedMotion.NOTHING);
            } else {

            }
            return true;
        });

        stopPedal.setOnTouchListener((View v, MotionEvent event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
//                Log.d("stop button", "down");
                stopPedal.setBackgroundResource(R.drawable.pedal_brea02);
                bluetoothConnectThread.setMotion(SpeedMotion.STOP);
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
//                Log.d("stop button", "up");
                stopPedal.setBackgroundResource(R.drawable.pedal_brea01);
                bluetoothConnectThread.setMotion(SpeedMotion.NOTHING);
            } else {

            }
            return true;
        });


    }

    @Override
    public boolean onTouch(final View v, MotionEvent event) {
        final float xc = wheel.getWidth() / 2;
        final float yc = wheel.getHeight() / 2;
        final float x = event.getX();
        final float y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                mCurrAngle = Math.toDegrees(Math.atan2(x - xc, yc - y));

                mPrevAngle = mCurrAngle;
                // wheel.clearAnimation();
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                mPrevAngle = mCurrAngle;
                mCurrAngle = Math.toDegrees(Math.atan2(x - xc, yc - y));

                moveAngle = mCurrAngle - mPrevAngle;
                if (moveAngle > 0) { // 오른쪽 회전
                    if (nowAngle + moveAngle > 90) {
                        break;
                    }
                } else { // 왼쪽 회전
                    if (nowAngle + moveAngle < -90) {
                        break;
                    }
                }
//                animate(mPrevAngle, mCurrAngle, 0); // 한번에 이동
                animate(nowAngle, nowAngle + moveAngle, 0); // 한번에 이동
//                Log.d("on Touch", "angle[" + mCurrAngle + "][" + mPrevAngle + "][" + nowAngle + "][" + moveAngle + "]");
                nowAngle = nowAngle + moveAngle;
                bluetoothConnectThread.setAngle(nowAngle);

                break;
            }
            case MotionEvent.ACTION_UP : {
//                mCurrAngle = Math.toDegrees(Math.atan2(x - xc, yc - y));
                break;
            }
        }
        return true;
    }

    private void animate(double fromDegrees, double toDegrees, long durationMillis) {
        final RotateAnimation rotate = new RotateAnimation((float) fromDegrees, (float) toDegrees,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(durationMillis);
        rotate.setFillEnabled(true);
        rotate.setFillAfter(true);
        wheel.startAnimation(rotate);
    }


    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
        if(Build.VERSION.SDK_INT >= 10){
            try {
                final Method m = device.getClass().getMethod("createInsecureRfcommSocketToServiceRecord", new Class[] { UUID.class });
                return (BluetoothSocket) m.invoke(device, MY_UUID);
            } catch (Exception e) {
                Log.e("bluetooth socket err", "Could not create Insecure RFComm Connection",e);
            }
        }
        return  device.createRfcommSocketToServiceRecord(MY_UUID);
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.d("onResume", "...onResume - try connect...");

        // Set up a pointer to the remote node using it's address.
        BluetoothDevice device = btAdapter.getRemoteDevice(address);

        // Two things are needed to make a connection:
        //   A MAC address, which we got above.
        //   A Service ID or UUID.  In this case we are using the
        //     UUID for SPP.

        try {
            btSocket = createBluetoothSocket(device);
        } catch (IOException e) {
            errorExit("Fatal Error", "In onResume() and socket create failed: " + e.getMessage() + ".");
        }

        // Discovery is resource intensive.  Make sure it isn't going on
        // when you attempt to connect and pass your message.
        btAdapter.cancelDiscovery();

        // Establish the connection.  This will block until it connects.
        Log.d("on resume bluetooth", "...Connecting...");
        try {
            btSocket.connect();
            Log.d("on resume bluetooth", "Connect ok");
        } catch (IOException e) {
            try {
                btSocket.close();
            } catch (IOException e2) {
                errorExit("Fatal Error", "In onResume() and unable to close socket during connection failure" + e2.getMessage() + ".");
            }
        }

        // Create a data stream so we can talk to server.
        Log.d("bluetooth connected", "...Create Socket...");

        bluetoothConnectThread = new BluetoothConnectThread(btSocket);
        bluetoothConnectThread.start();
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.d("on pause", "...In onPause()...");

        try {
            btSocket.close();
        } catch (IOException e2) {
            errorExit("Fatal Error", "In onPause() and failed to close socket." + e2.getMessage() + ".");
        }
    }

    private void checkBTState() {
        // Check for Bluetooth support and then check to make sure it is turned on
        // Emulator doesn't support Bluetooth and will return null
        if(btAdapter == null) {
            errorExit("Fatal Error", "Bluetooth not support");
        } else {
            if (btAdapter.isEnabled()) {
                Log.d("bluetooth enabled", "...Bluetooth ON...");
            } else {
                //Prompt user to turn on Bluetooth
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }
    }

    private void errorExit(String title, String message){
        Toast.makeText(getBaseContext(), title + " - " + message, Toast.LENGTH_LONG).show();
        finish();
    }

    class CarSpeedThread extends Thread {

        private int carSpeed;
        private SpeedMotion motionType;
        private double carAngle;

        public CarSpeedThread() {
            motionType = SpeedMotion.NOTHING;
            carAngle = 0;
        }

        public void setMotion(SpeedMotion motionType) {
            this.motionType = motionType;
        }

        public void setAngle(double carAngle) {
            this.carAngle = carAngle;
        }


        @Override
        public void run() {
            super.run();

            while (true) {
                try {
                    if (carSpeed == 0 && motionType == SpeedMotion.NOTHING) {
                            Thread.sleep(100);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (motionType == SpeedMotion.ACCELERATE) {
                    this.carSpeed += 5;
                } else if (motionType == SpeedMotion.STOP) {
                    this.carSpeed -= 10;
                } else {
                    this.carSpeed -= 3;
                }
                if (this.carSpeed > 255) {
                    this.carSpeed = 255;
                } else if (this.carSpeed < 0) {
                    this.carSpeed = 0;
                }

                StringBuffer sb = new StringBuffer();

                long leftSpeed, rightSpeed;
                double changeSpeed;
                changeSpeed = (carSpeed / 255.0) * 255.0;
                if (carAngle == 0) {
                    // 자동차 왼쪽 바퀴 1 * 스피드
                    // 자동차 오른쪽 바퀴 1 * 스피드
                    leftSpeed = Math.round(changeSpeed);
                    rightSpeed = Math.round(changeSpeed);

                } else if (carAngle > 0) { // 오른쪽 회전
                    // 자동차 왼쪽 바퀴 1 * 스피드
                    // 자동차 오른쪽 바퀴 ((90-각도)/90) * 스피드
                    leftSpeed = Math.round(changeSpeed);
                    rightSpeed = (int) (( (90.0 - carAngle) / 90.0) * Math.round(changeSpeed) );
                } else { // 왼쪽 회전
                    // 자동차 오른쪽 바퀴 1 * 스피드
                    // 자동차 왼쪽 바퀴 ((90+각도)/90) * 스피드
                    rightSpeed = Math.round(changeSpeed);
                    leftSpeed = Math.round ( ( (90.0 + carAngle) / 90.0) * Math.round(changeSpeed));
                }
                sb.append(String.format("L%03dR%03d", leftSpeed, rightSpeed));

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


                Log.d("Car Speed", "[" + this.carSpeed + "]change[" + changeSpeed + "][" + sb.toString() + "]");
            }
        }
    }
}
